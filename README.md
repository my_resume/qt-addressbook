Проект Qt-AddressBook - приложение для работы с контактами в базе данных sqlite.

Программа реализована с использованием исходного кода из репозитория <https://github.com/smay1613/Qt-AddressBook/tree/addressbook_databases> с внесёнными изменениями:
* убраны ненужные зависимости в заголовочных файлах и исходниках;
* в sql запросе тип Surname изменён с INTEGER на TEXT в файле ConnectionManager.cpp;
* убран класс Processor из файла contactsreader.h, зависимость от Processor.h перенесена из contactsreader.cpp в contactsreader.h;
* добавлены три поля ввода и кнопка с функцией добавления новых контактов в базу данных;
* тексты сообщений на русском языке.

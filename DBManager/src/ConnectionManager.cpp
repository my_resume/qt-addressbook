/*
Проект включает в себя изменённый исходный код, содержащийся в репозитории <https://github.com/smay1613/Qt-AddressBook/tree/addressbook_databases>
от автора Дмитрия Афанасьева с его согласия, выраженного в видео <https://www.youtube.com/watch?v=rzlJq_njIDw>.

Этот проект использует функционал модулей Qt Toolkit в соответствии с условиями GNU General Public License version 3.

Copyright © 2021 Александр Кубраков.

This file is part of Qt-AddressBook.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version approved by the KDE Free Qt Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#include "ConnectionManager.h"
#include <QSqlError>
#include <QSqlQuery>
#include <QStandardPaths>
#include <QDir>

using namespace DBTypes;
namespace db
{
namespace
{
class DBCloser {
public:
    void operator() (QSqlDatabase* db) {
        db->close();
        QSqlDatabase::removeDatabase(QSqlDatabase::defaultConnection);
    }
};
}

class ConnectionManager::DBManagerPrivate {
public:
    std::unique_ptr<QSqlDatabase, DBCloser> m_database;
    std::string m_dbPath;
    DBState m_state {DBState::OK};
    bool m_isValid {true};

    bool setUp();
    bool setUpWorkspace();
    bool setUpTables();

    void setIsValid(bool isValid);
};

ConnectionManager& ConnectionManager::instance()
{
    static ConnectionManager instance {};
    return instance;
}

std::string ConnectionManager::databasePath() const
{
    return m_d->m_dbPath;
}

DBState ConnectionManager::state() const
{
    return m_d->m_state;
}

bool ConnectionManager::DBManagerPrivate::setUp()
{
    const QString driver {"QSQLITE"};

    if (!QSqlDatabase::isDriverAvailable(driver))
    {
        m_state = DBState::ERROR_NO_DRIVER;
        qWarning() << "Драйвер " << driver << " не доступен.";
        return false;
    }

    if (!setUpWorkspace())
    {
        m_state = DBState::ERROR_WORKSPACE;
        qCritical() << "Настройка рабочей области не удалась!";
        return false;
    }

    auto* db = new QSqlDatabase {QSqlDatabase::addDatabase(driver)};
    m_database.reset(db);
    m_database->setDatabaseName(QString::fromStdString(m_dbPath));

    qDebug() << "Название базы данных: " << m_database->databaseName();

    if (!m_database->open())
    {
        m_state = DBState::ERROR_OPENING;
        qCritical() << "Ошибка при открытии базы данных " << m_database->databaseName()
                    << " причина: " <<  m_database->lastError().text();
        return false;
    }

    return setUpTables();
}

bool ConnectionManager::DBManagerPrivate::setUpWorkspace()
{
#ifdef BUILD_TESTS
    const QString databaseName {"TestDB"};
#else
    const QString databaseName {"ContactsDB"};
#endif
    const QString location {QStandardPaths::writableLocation(QStandardPaths::AppDataLocation)};
    const QString fullPath {location + "/" + databaseName};

    m_dbPath = fullPath.toStdString();

    QDir dbDirectory {location};
    if (!dbDirectory.exists())
    {
        const bool creationResult {dbDirectory.mkpath(location)};
        qWarning() << "Директория для базы данных не существует, результат создания: "
                   << creationResult;
    }

    qDebug() << "Путь к данным: " << fullPath;

    return dbDirectory.exists();
}

bool ConnectionManager::DBManagerPrivate::setUpTables()
{
    bool result {true};

    std::vector<QSqlQuery> creationQueries = {
        QSqlQuery {
            "CREATE TABLE IF NOT EXISTS Contacts"
            "("
            "Name TEXT,"
            "Surname TEXT,"
            "PhoneNumber TEXT,"
            "UNIQUE(Name, Surname)"
            ")"
        }
    };

    for (auto& query : creationQueries)
    {
        if (!query.exec())
        {
            result = false;
            m_state = DBState::ERROR_TABLES;
            qWarning() << "Создание таблицы не удалось. Причина: "
                       << query.lastError();
        }
        else
        {
            qWarning() << "Если таблица Contacts не существовала, она была создана. Запрос: \n" << query.lastQuery();
        }
    }

    return result;
}

void ConnectionManager::DBManagerPrivate::setIsValid(bool isValid)
{
    m_isValid = isValid;
}

bool ConnectionManager::isValid() const
{
    return m_d->m_isValid && m_d->m_database->isValid();
}

ConnectionManager::ConnectionManager()
    : m_d {new DBManagerPrivate {}}
{
    const bool setupResult {m_d->setUp()};
    m_d->setIsValid(setupResult);
}

ConnectionManager::~ConnectionManager()
{
}
}

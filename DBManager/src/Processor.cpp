/*
Проект включает в себя изменённый исходный код, содержащийся в репозитории <https://github.com/smay1613/Qt-AddressBook/tree/addressbook_databases>
от автора Дмитрия Афанасьева с его согласия, выраженного в видео <https://www.youtube.com/watch?v=rzlJq_njIDw>.

Этот проект использует функционал модулей Qt Toolkit в соответствии с условиями GNU General Public License version 3.

Copyright © 2021 Александр Кубраков.

This file is part of Qt-AddressBook.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version approved by the KDE Free Qt Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#include "Processor.h"
#include "Manipulator.h"
#include "Selector.h"
#include "dbmapper.h"
#include <mutex>

namespace db
{
struct Processor::ProcessorPrivate
{
    Manipulator manipulator;
    Selector selector;
#ifdef INSERT_TEST_DATA
    void insertMockData();
    std::once_flag initialized;
#endif
};

#ifdef INSERT_TEST_DATA
void Processor::ProcessorPrivate::insertMockData()
{
    manipulator.insertRow("Contacts", {{"FirstName", "SecondName", "79123456789"}});
    manipulator.insertRow("Contacts", {{"Asher", "Blankenship", "79987654321"}});
    manipulator.insertRow("Contacts", {{"Eric", "French", "79567894321"}});
    manipulator.insertRow("Contacts", {{"Michael", "Cooper", "380999986699"}});
    manipulator.insertRow("Contacts", {{"Ulric", "Shepherd", "380626465726"}});
    manipulator.insertRow("Contacts", {{"Lionel", "Holt", "380454835415"}});
    manipulator.insertRow("Contacts", {{"Aquila", "Tanner", "380813526568"}});
    manipulator.insertRow("Contacts", {{"Lee", "Booth", "380423227258"}});
    manipulator.insertRow("Contacts", {{"Drew", "Branch", "380955112745"}});
    manipulator.insertRow("Contacts", {{"Francis", "Buckley", "380658457417"}});
    manipulator.insertRow("Contacts", {{"Stuart", "Wright", "380813526568"}});
    manipulator.insertRow("Contacts", {{"Leonard", "Tanner", "380813526568"}});
    manipulator.insertRow("Contacts", {{"Christopher", "Dillon", "380565828659"}});
    manipulator.insertRow("Contacts", {{"", "Ivanov", "380501728834"}});
}
#endif

db::Processor::Processor()
    : m_d {new ProcessorPrivate {}}
{
#ifdef INSERT_TEST_DATA
    auto inserter = [this] {
        m_d->insertMockData();
    };
    std::call_once(m_d->initialized, inserter);
#endif
}

Processor::~Processor()
{
}

std::pair<DBTypes::DBResult, std::vector<DBTypes::DBEntry>> Processor::requestTableData(DBTypes::DBTables table)
{
    std::vector<QVariantList> result;
    const DBTypes::DBResult resultState {m_d->selector.selectAll(tableMapper.at(table), result)};
    return std::make_pair(resultState, std::move(result));
}

DBTypes::DBResult Processor::addNewContact(DBTypes::DBTables tableName, const QVariantList &rowData)
{
    int lastInsertId;
    DBTypes::DBResult resultState{DBTypes::DBResult::OK};
    std::tie(resultState, lastInsertId)= m_d->manipulator.insertRow(tableMapper.at(tableName), rowData);
    return resultState;
}
}

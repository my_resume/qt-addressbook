/*
Проект включает в себя изменённый исходный код, содержащийся в репозитории <https://github.com/smay1613/Qt-AddressBook/tree/addressbook_databases>
от автора Дмитрия Афанасьева с его согласия, выраженного в видео <https://www.youtube.com/watch?v=rzlJq_njIDw>.

Этот проект использует функционал модулей Qt Toolkit в соответствии с условиями GNU General Public License version 3.

Copyright © 2021 Александр Кубраков.

This file is part of Qt-AddressBook.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version approved by the KDE Free Qt Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#include "Executor.h"
#include <QSqlError>

using namespace DBTypes;

namespace db
{
Executor::Executor()
    : m_connectionManager {ConnectionManager::instance()}
{

}

std::pair<DBResult, QSqlQuery> Executor::execute(const std::string& queryText, const QVariantList& args)
{
    if (!m_connectionManager.isValid())
    {
        qCritical() << "База данных повреждена, пропуск";
        return std::make_pair(DBResult::FAIL, QSqlQuery {});
    }
    QSqlQuery query {QString::fromStdString(queryText)};

    for(int i = 0; i < args.size(); i++)
    {
        query.bindValue(i, args[i]);
    }

    DBResult result {DBResult::OK};

    if(!query.exec() && query.lastError().isValid())
    {
        qCritical() << query.lastError().text()<< query.lastQuery();
        result = DBResult::FAIL;
    }

    return std::make_pair(result, query);
}
}

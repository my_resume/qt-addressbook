/*
Проект включает в себя изменённый исходный код, содержащийся в репозитории <https://github.com/smay1613/Qt-AddressBook/tree/addressbook_databases>
от автора Дмитрия Афанасьева с его согласия, выраженного в видео <https://www.youtube.com/watch?v=rzlJq_njIDw>.

Этот проект использует функционал модулей Qt Toolkit в соответствии с условиями GNU General Public License version 3.

Copyright © 2021 Александр Кубраков.

This file is part of Qt-AddressBook.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version approved by the KDE Free Qt Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#include "contactsreader.h"

using namespace DBTypes;

ContactsReader::ContactsReader()
    : m_dbProcessor {new db::Processor {}}
{
}

std::vector<Contact> transform(const std::vector<DBEntry>& source)
{
    std::vector<Contact> target;
    std::transform(source.begin(), source.end(), std::back_inserter(target),
                   [](const DBEntry& entry) {
        return Contact {entry[1].toString(), entry[2].toString(), entry[3].toString(), entry[0].toInt()};
    });
    return target;
}

std::pair<bool, std::vector<Contact>> ContactsReader::requestContactsBrowse()
{
    DBResult result;
    std::vector<DBEntry> entries;
    std::tie(result, entries) = m_dbProcessor->requestTableData(DBTables::Contacts);
    return std::make_pair(result == DBResult::OK,
                          transform(entries));
}

ContactsReader::~ContactsReader()
{
}

DBResult ContactsReader::addNewContact(const QVariantList &rowData)
{
    DBTypes::DBResult resultState{DBTypes::DBResult::OK};
    resultState= m_dbProcessor->addNewContact(DBTables::Contacts, rowData);
    return resultState;
}

/*
Проект Qt-AddressBook - приложение для работы с контактами в базе данных sqlite.

Программа реализована с использованием исходного кода из репозитория <https://github.com/smay1613/Qt-AddressBook/tree/addressbook_databases> с внесёнными изменениями:
* убраны ненужные зависимости в заголовочных файлах и исходниках;
* в sql запросе тип Surname изменён с INTEGER на TEXT в файле ConnectionManager.cpp;
* убран класс Processor из файла contactsreader.h, зависимость от Processor.h перенесена из contactsreader.cpp в contactsreader.h;
* добавлены три поля ввода и кнопка с функцией добавления новых контактов в базу данных;
* тексты сообщений на русском языке.

Проект включает в себя изменённый исходный код, содержащийся в репозитории <https://github.com/smay1613/Qt-AddressBook/tree/addressbook_databases>
от автора Дмитрия Афанасьева с его согласия, выраженного в видео <https://www.youtube.com/watch?v=rzlJq_njIDw>.

Этот проект использует функционал модулей Qt Toolkit в соответствии с условиями GNU General Public License version 3.

Copyright © 2021 Александр Кубраков.

This file is part of Qt-AddressBook.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version approved by the KDE Free Qt Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include "contactsmodel.h"

int main(int argc, char *argv[])
{
#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
#endif

    QGuiApplication app(argc, argv);

    qmlRegisterType<ContactsModel>("Contacts", 1, 0, "ContactsModel");

    QQmlApplicationEngine engine;
    engine.addImportPath(":/qml");
    const QUrl url(QStringLiteral("qrc:/main.qml"));
    QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
                     &app, [url](QObject *obj, const QUrl &objUrl) {
        if (!obj && url == objUrl)
            QCoreApplication::exit(-1);
    }, Qt::QueuedConnection);
    engine.load(url);

    return app.exec();
}

/*
Проект включает в себя изменённый исходный код, содержащийся в репозитории <https://github.com/smay1613/Qt-AddressBook/tree/addressbook_databases>
от автора Дмитрия Афанасьева с его согласия, выраженного в видео <https://www.youtube.com/watch?v=rzlJq_njIDw>.

Этот проект использует функционал модулей Qt Toolkit в соответствии с условиями GNU General Public License version 3.

Copyright © 2021 Александр Кубраков.

This file is part of Qt-AddressBook.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version approved by the KDE Free Qt Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
import QtQuick 2.11
import ContactsModule.Impl 1.0
import StyleModule 1.0
import QtQuick.Controls 2.15
import QtQuick.Layouts 2.15
ApplicationWindow {
    id: root
    visible: true
    width: 480
    height: 640
    title: qsTr("Address book")

    ContactsView {
        id: _contactsView
        anchors.fill: parent
    }

    Rectangle {
        id: _background
        z: -100
        anchors.fill: parent
        color: Style.backgroundColor
        opacity: Style.emphasisOpacity
    }


    footer: Pane {
        RowLayout {
            width: parent.width
            TextField {
                id: _name
                placeholderText: qsTr("Имя")
            }
            TextField {
                id: _surname
                placeholderText: qsTr("Фамилия")
            }
            TextField {
                id: _phonenumber
                placeholderText: qsTr("Номер телефона")
            }

            Button {
                text: qsTr("Добавить")

                onClicked:  {
                    _contactsView.model.addNewContact(_name.text.trim(), _surname.text.trim(), _phonenumber.text.trim())
                    _name.clear()
                    _surname.clear()
                    _phonenumber.clear()
                }

            }
        }
    }


}

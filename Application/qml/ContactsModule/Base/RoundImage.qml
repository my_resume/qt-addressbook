import QtQuick 2.0
import StyleModule 1.0

Image {
    id: root
    property bool rounded: true
    property bool adapt: true

    layer.enabled: rounded
}
